// import Node's built-in HTTP module, a set of tools that can handle server requests and responses, etc.
let http = require("http")

const server = http.createServer(function (request, response) {
    /* Terminology note:
        - URL: localhost:4000/greeting
        - Origin: localhost:4000
        - Endpoint: /greeting
    */
    if (request.url === "/greeting") {
        response.writeHead(200, {"Content-Type": "text/plain"});

        response.end("Hello Amer!");
    } else if (request.url === "/") { // a single slash means no endpoint was added to the URL/origin.
        response.writeHead(200, {"Content-Type": "text/plain"});

        response.end("This is the Homepage");
    } else {
        response.writeHead(404, {"Content-Type": "text/plain"});

        response.end("Page not found");
    }
});


const port = 4000;
server.listen(port);
console.log(`server running at port ${port}`);