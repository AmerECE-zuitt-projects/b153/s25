// import Node's built-in HTTP module, a set of tools that can handle server requests and responses, etc.
let http = require("http")

// the createServer() method lets you create an HTTP server that listen to requests on specified port and can also send responses back to the client.
// User sends a request to view a page on port 4000 -> createServer() reacts to this request by executing our code, which sends the response.
const server = http.createServer(function (request, response) {
    // Use the writeHead method to:
    // Set a status code for the response - 200 means OK
    // Set the content-type of the response as a plain text message
    response.writeHead(200, {"Content-Type": "text/plain"});

    // send a response with the text content
    response.end("Hello Amer!");
});

/* port numbers:
    0 - 1023: well-known ports are kept reserved/never used because they are typically used for connections important for the system
    1024 - 65535 can be used
    - if an application is running on port 4000, no other application 
*/
const port = 4000;
// if a user tries to access port 4000, send the response that we added in createServer.
server.listen(port);
// show a confirmation message that the server is running on the assigned port.
console.log(`server running at port ${port}`);